# 趣玩python系列六 整站图片一键下载

# 声明： 本项目代码只做为学习使用，任何商业用途非作者意愿，请勿知法犯法

#### 介绍
整站图片一键下载

本组织所有项目都趣玩Python系列子项目，所有代码为原创，或标记转载，如有侵权，请邮件mtong@foxmail.com进行告知
软件版本 python3.7.4

#### 软件架构
download_image.py 一键下载全站图片

download_image_page.py 下载单页所有图片


#### 安装教程
pip3 install requests


#### 使用说明

python3 download_image.py -i https://www.baidu.com -n 10

警告 默认采集是10个页面的所有图片，如果设置无限大，可能会锁死，当前未处理强制结束进程功能，可以用任务管理器进行结束


python3 download_image_page.py https://www.baidu.com



#### 作者相关：

[博客](https://www.mylasting.com)、[新浪微博](https://weibo.com/u/6008105952)、[简书](https://www.jianshu.com/u/0c790ada8f1f)

**本系列教程及源码地址：[点击访问](https://gitee.com/python_play/study_python/)**

**最后：如果你正在学习Python的路上，或者准备打算学习Python、明哥会陪着你陪你一起共同进步！**

**手打不易，有用的话，请记得关注转发。**